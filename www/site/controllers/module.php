<?php

return function($site, $pages, $page) {

  $query   = get('q');
  $results = $site->index()->listed()->search($query, 'title|text|story|description');
  $results = $results->paginate(20);

  return array(
    'query'      => $query,
    'results'    => $results,
    'pagination' => $results->pagination()
  );

};
