# fractal-chronicles-de
Website for instructions and all cards for the tabletop-boardgame.

> **Fractal Chronicles:**
Tales from the journeys to explore the broken lands

## Contents
1. Quickstart
2. Install
3. Frontend
4. Kirby (localhost)
5. Documentations
6. Upcoming Features / Dependencies
7. Features / Dependencies

## Quickstart

- Install `git clone --recursive https://gitlab.com/nihililix/fractal-chronicles.git` the repository
- install dependencies `yarn`
- build frontend `yarn build`
- or serve with `yarn serve` and `php -S localhost:8000 kirby/router.php` 

## Install

##### Installation 
Install default files and kirby as git submodules
```
git clone --recursive https://gitlab.com/nihililix/fractal-chronicles.git
```

or 
```
git submodule init 
git submodule update
```

## Frontend

##### Installation
```
yarn
```

##### Build Prod Version
```
yarn build
```

##### Build Dev Version
```
yarn serve
```
## Kirby

##### Running from Command Line 
go in project folder 'www'
```
php -S localhost:8000 kirby/router.php
```
http://localhost:8000

## Documentations
- [Frontend](./docs/FRONTEND.md)

## Features / Dependencies

* **Kirby (CMS)**
* scss
* es6
* webpack 4
* stylelint
* eslint
* browserlist
* fractal-chronicles-content

## Upcoming Features / Dependencies

work in progress
